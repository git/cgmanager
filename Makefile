RELEASE=4.0

# simple backport from debian sid

PACKAGE=cgmanager
PKGVER=0.39
DEBREL=pve1

# also update changelog

PKGSRC=${PACKAGE}.tgz
PKGDIR=${PACKAGE}

ARCH:=$(shell dpkg-architecture -qDEB_BUILD_ARCH)
GITVERSION:=$(shell cat .git/refs/heads/master)
SOURCETXT="git clone git://git.proxmox.com/git/pve/cgmanager.git\\ngit checkout ${GITVERSION}"

DEBS=							\
lib${PACKAGE}-dev_${PKGVER}-${DEBREL}_${ARCH}.deb	\
lib${PACKAGE}0_${PKGVER}-${DEBREL}_${ARCH}.deb		\
${PACKAGE}_${PKGVER}-${DEBREL}_${ARCH}.deb

all: ${DEBS}

.PHONY: dinstall
dinstall: ${DEBS}
	dpkg -i ${DEBS}

.PHONY: deb
deb ${DEBS}: ${PKGSRC}
	rm -rf ${PKGDIR}
	tar xf ${PKGSRC}
	cp -a debian ${PKGDIR}/debian
	echo "${SOURCETXT}" > ${PKGDIR}/debian/SOURCE
	echo "debian/SOURCE" >>${PKGDIR}/debian/docs
	cd ${PKGDIR}; dpkg-buildpackage -b -uc -us
	lintian ${DEBS}

.PHONY: download
download ${PKGSRC}:
	rm -rf ${PKGDIR} ${PKGSRC}
	git clone git://github.com/lxc/cgmanager
	tar czf ${PKGSRC}.tmp ${PKGDIR}
	mv ${PKGSRC}.tmp ${PKGSRC}

.PHONY: upload
upload: ${DEBS}
	umount /pve/${RELEASE}; mount /pve/${RELEASE} -o rw 
	mkdir -p /pve/${RELEASE}/extra
	rm -rf /pve/${RELEASE}/extra/lib${PACKAGE}-dev_*.deb
	rm -rf /pve/${RELEASE}/extra/lib${PACKAGE}0_*.deb
	rm -rf /pve/${RELEASE}/extra/${PACKAGE}_*.deb
	rm -rf /pve/${RELEASE}/extra/Packages*
	cp ${DEBS} /pve/${RELEASE}/extra
	cd /pve/${RELEASE}/extra; dpkg-scanpackages . /dev/null > Packages; gzip -9c Packages > Packages.gz
	umount /pve/${RELEASE}; mount /pve/${RELEASE} -o ro

.PHONY: clean
clean:
	rm -rf *~ *_${ARCH}.deb *_all.deb *_${ARCH}.udeb *.changes *.dsc ${PKGDIR}
